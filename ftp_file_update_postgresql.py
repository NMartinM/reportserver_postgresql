# ftptest.py
 
import os
import sqlite3
import time
from ftplib import FTP
from datetime import datetime
import psycopg2

from os import walk

FILEPATH = ""

storedir=FILEPATH+'Temp'
plc_ip = "192.168.10.79"

#Get all files in Temp directory
f = []
for root, dirs, files in os.walk(storedir):  
    for filename in files:
        f.append(filename)

#Sort obtained filenames in Temp directory
f.sort(key=lambda s: os.path.getmtime(os.path.join(storedir, s)), reverse=False)

listing_order_to_download = []

#Open DB
try:
    conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='huber'")
    cur = conn.cursor()
except:
    print "I am unable to connect to the database"

#DB insertion
for file in f:
    print file
    current_file_name = file[0:file.find("_")]
    with open(storedir+"/"+file) as fp:  
        line = fp.readline()
        print "Inserting data from file: "+file+" **********"
        while line: 
            #print "First "+line
            split_line = line.split(";")
            if "\n" not in split_line[0]:
                #print "Split_line_time "+split_line[0]
                date_to_insert = datetime.strptime(split_line[0], '%Y-%m-%d %H:%M:%S')
                #Pulses files
                if current_file_name.lower()=="pulses":
                    cur.execute('INSERT INTO pulses(plc_ip,timestamp,output_number,ration,pulsegiven,rationok,food) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unipu DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4],""))
                    print "Pulses: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                         
                #Rates files
                elif current_file_name.lower()=="rates":
                    print "Inserting row with date into rates: "+split_line[0]
                    cur.execute('INSERT INTO rates(plc_ip,timestamp,user_,output_number,rate,ratebefore) \
                    VALUES (%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unira DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4]))
                    print "Rates: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                

                #Rations files
                elif current_file_name.lower()=="rations":
                    print "Inserting row with date into rations: "+split_line[0]
                    cur.execute('INSERT INTO \
                    rations(plc_ip,timestamp,user_,output_number,ration,starttime,endtime,rationamount,stoptime,cycleamount,cyclepulses,timebetweenpulses,percentage) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unirat DO NOTHING'\
                    ,(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4],split_line[5],split_line[6],split_line[7],split_line[8],split_line[9],split_line[10],split_line[11]))
                    print "Rations: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
            

                #Estadofp files
                elif current_file_name.lower()=="estadofp":
                    print "Inserting row with date into estadofp: "+split_line[0]
                    cur.execute('INSERT INTO estadofp(plc_ip,timestamp,user_,output_number,automatic,onprogram,photocell,onok,offok,engagedwith) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unifp DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4],split_line[5],split_line[6],split_line[7],split_line[8]))
                    print "Estadofp: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                    

                #States files
                elif current_file_name.lower()=="states":
                    print "Inserting row with date into states: "+split_line[0]
                    cur.execute('INSERT INTO states(plc_ip,timestamp,user_,output_number,ration,onoff,rationstop,pondok,rationactive,fpok) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unist DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4],split_line[5],split_line[6],split_line[7],split_line[8]))
                    print "States: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                    

                #Foodsa files
                elif current_file_name.lower()=="foodsa":
                    print "Inserting row with date into foodsa: "+split_line[0]
                    cur.execute('INSERT INTO foodsa(plc_ip,timestamp,user_,name,company,size,medicated) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unifsa DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4],split_line[5]))
                    print "Foodsa: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
            

                #Resets files
                elif current_file_name.lower()=="resets":
                    print "Inserting row with date into resets: "+split_line[0]
                    cur.execute('INSERT INTO resets(plc_ip,timestamp,user_,output_number) \
                    VALUES (%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unires DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2]))
                    print "Resets: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                    

                #Foodslots files
                elif current_file_name.lower()=="foodslots":
                    print "Inserting row with date into foodslots: "+split_line[0]
                    cur.execute('INSERT INTO foodslots(plc_ip,timestamp,user_,output_number,food,lot) \
                    VALUES (%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT unifs DO NOTHING',(plc_ip,date_to_insert,split_line[1],split_line[2],split_line[3],split_line[4]))   
                    print "Foodslots: Inserting row with date: "+split_line[0]
                    line = fp.readline()
                    conn.commit() #Commit changes to DB.
                    
                elif current_file_name.lower()=="lightprog":
                    line = fp.readline()     
            else:
                line = fp.readline()


    #print""
    conn.commit() #Commit changes to DB.
cur.close() 
#Close DB
conn.close()
