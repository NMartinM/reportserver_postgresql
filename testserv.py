from flask import Flask, request, jsonify, json
from datetime import datetime
from datetime import timedelta
import os
import sqlite3

application = Flask(__name__)

@application.route("/")
def hello():
    return "<h1 style='color:blue'>Hello There!</h1>"

@application.route('/hola', methods=['POST'])
def hola():
	#Open DB
    db = sqlite3.connect('reports_database')
    db.isolation_level = None
    db.execute('pragma journal_mode=wal')
    cursor = db.cursor()
    print (request.is_json)
    content = request.get_json()
    js = json.loads(request.get_data())
    print "***************************************************"
    print (js['Estanque']['SAVE'])
    print ""
    #print js['Config'][0]
    print "***************************************************"
    data = js['Estanque']['DATA'].split(';')
    cursor.execute('INSERT OR IGNORE INTO pulses(plc_ip,timestamp,OUTPUT_NUMBER,ration,pulseGiven,rationOk,food) \
                                VALUES (?,?,?,?,?,?,?)',(js['PLC'],js['Estanque']['TIMESTAMP'],js['Estanque']['ID'],data[0],data[1],data[2],""))
    db.commit() #Commit changes to DB.
    db.close() #Close DB.
    return 'ok'

if __name__ == "__main__":
    application.run(host='0.0.0.0',debug=True)
