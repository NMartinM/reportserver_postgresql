# ftptest.py
 
import os
import sqlite3
import time
from datetime import datetime
import psycopg2

from os import walk

FILEPATH = ""

 
while True:
     #Open DB
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    #Table pulses food column update with table foodslots
    date_to_update_from = datetime.strptime("2100-01-01 23:59:59", '%Y-%m-%d %H:%M:%S')

    #Check if rows without food exist
    cur.execute('SELECT timestamp FROM pulses WHERE food=\'\';')
    no_food_date = cur.fetchone()
    if no_food_date:
        no_food_date = str(no_food_date[0])
        #print "nofooddate"+no_food_date
    else:
        no_food_date = None
        #print "None"
    cur.execute('select timestamp from foodslots;')
    check_foodslots = cur.fetchone()
    if check_foodslots:
        print "foodslots_exist"
    else:
        no_food_date = None
    #If rows without food exist, update.
    if no_food_date is not None:
        #Obtain all rows from that date onwards.
        #cursor.execute('SELECT * FROM pulses WHERE timestamp>=\"'+no_food_date+'\";')
        cur.execute('SELECT * FROM pulses WHERE food=\'\' order by output_number;')
        to_be_updated = cur.fetchall()
        #no_food_date_dt = datetime.strptime(no_food_date, '%Y-%m-%d %H:%M:%S')
        #Obtain date with food. The date must be before pulses date.
        cur.execute('select timestamp from foodslots where timestamp<=\''+no_food_date+'\' order by timestamp desc limit 1;')
        date_to_insert_from = cur.fetchone()
        if date_to_insert_from:
            date_to_insert_from = date_to_insert_from[0]
        else:
            date_to_insert_from = None
            cur.execute('select timestamp from foodslots order by timestamp asc limit 1;')
            date_to_insert_from = str(cur.fetchone()[0])
            #print date_to_insert_from
        if date_to_insert_from is not None:    
            #Obtain all rows from that date onwards.
            cur.execute('select timestamp,food,lot,output_number from foodslots where timestamp >=\''+date_to_insert_from+'\' order by timestamp;')
            to_update_from = cur.fetchall()
            #print to_update_from
            i=0
            date_to_update_from = None
            last = 0
            #For every element to be updated........
            for element_to_update in to_be_updated:
                cur.execute('select timestamp,food,lot,output_number from foodslots where timestamp >=\''+date_to_insert_from+'\' and output_number=\''+str(element_to_update[3])+'\' order by timestamp;')
                to_update_from = cur.fetchall()
                if to_update_from: 
                    #print to_update_from
                    to_update_from = to_update_from
                else:
                    to_update_from=None
                i=0
                last=0
                while True:
                    if to_update_from is None:
                        break
                    output_number_to_update = element_to_update[3]
                    date_to_update = datetime.strptime(str(element_to_update[2]), '%Y-%m-%d %H:%M:%S')
                    if last==0:
                        if to_update_from[i][0] is not None: #If exists
                            date_to_update_from = datetime.strptime(str(to_update_from[i][0]), '%Y-%m-%d %H:%M:%S')
                            if len(to_update_from)>1 and last==0:
                                date_to_update_from_next = datetime.strptime(str(to_update_from[i+1][0]), '%Y-%m-%d %H:%M:%S')
                            else:
                                date_to_update_from_next = datetime.strptime("2100-01-01 23:59:59", '%Y-%m-%d %H:%M:%S')
                        else:
                            date_to_update_from = None
                    #If the date to update is between current foodslots date and the next
                    # if output_number_to_update==to_update_from[i][3]:
                        #print "*******"
                        #print str(output_number_to_update)+" "+str(to_update_from[i][3])
                        #print date_to_update_from
                        #print date_to_update
                        #print date_to_update_from_next
                        #print "****"
                    if date_to_update>=date_to_update_from and date_to_update<=date_to_update_from_next and output_number_to_update==to_update_from[i][3] or date_to_update_from is None:
                        #print "UPDATE"
                        #print date_to_update.strftime("%Y-%m-%d %H:%M:%S")+"--"+date_to_update_from.strftime("%Y-%m-%d %H:%M:%S")+"--"+str(element_to_update[0])
                        cur.execute("UPDATE pulses SET food=%s WHERE id=\'"+str(element_to_update[0])+"\'" ,((to_update_from[i][1].rstrip()+"/"+to_update_from[i][2].rstrip()).strip(),))
                        break
                    i=i+1
                    #Break before indexoutofbounds!
                    if (i+1) >= len(to_update_from):
                        #print "BREAK"
                        if date_to_update_from_next<date_to_update:
                            date_to_update_from=date_to_update_from_next
                            #print date_to_update_from
                            date_to_update_from_next = datetime.strptime("2100-01-01 23:59:59", '%Y-%m-%d %H:%M:%S')
                            last=1
                            #i=i-1
                        else:
                            last=0
                            break       
        else:
            print "No UPDATE"
    else:
        print "No UPDATE"
    conn.commit()
    conn.close()
    cur.close()
    time.sleep(5)
