from flask import Flask, request, jsonify, json
from datetime import datetime
from datetime import timedelta
import os
import sqlite3
import psycopg2

FILEPATH = ""

application = Flask(__name__)

@application.route('/listpulses/"<startdate>"-"<enddate>"')
def listpulses(startdate,enddate):
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select * from pulses where timestamp between \'"+startdate+"\' and \'"+enddate+"\'")
    rows = cur.fetchall()
    cur.close()
    conn.close() 
    return jsonify(rows)

@application.route('/listfoodslots/"<startdate>"-"<enddate>"')
def listfoodslots(startdate,enddate):
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select * from foodslots where timestamp between \'"+startdate+"\' and \'"+enddate+"\'")
    rows = cur.fetchall()
    cur.close()
    conn.close() 
    return jsonify(rows)

@application.route('/insertfoodslots/<plcip>;<timestamp>;<user>;<output_number>;<food>;<lot>')
def insertfoodslots(plcip,timestamp,user,output_number,food,lot):
    result=[]
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute('INSERT INTO foodslots(plc_ip,timestamp,user_,output_number,food,lot) VALUES (%s,%s,%s,%s,%s,%s)',(plcip,timestamp,user,output_number,food.strip(),lot))
    #rows = cursor.fetchall()  
    print "foodslots"
    conn.commit() #Commit changes to DB.
    cur.close()
    conn.close() #Close DB.

    result.append({'result':'ok'})
    return jsonify(result)

@application.route('/insertfoodsa/<plcip>;<timestamp>;<user>;<name>;<company>;<size>;<medicated>')
def insertfoodsa(plcip,timestamp,user,name,company,size,medicated):
    size = size.replace("_","/")
    result=[]
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    print plcip.strip(),timestamp.strip(),user.strip(),name.strip(),company.strip(),size.strip(),medicated.strip()
    cur.execute('INSERT INTO foodsa(plc_ip,timestamp,user_,name,company,size,medicated) VALUES (%s,%s,%s,%s,%s,%s,%s)',(plcip.strip(),timestamp.strip(),user.strip(),name.strip(),company.strip(),size.strip(),medicated.strip()))
    #rows = cursor.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    print "foodsa"
    result.append({'result':'ok'})
    return jsonify(result)

#Returns the sum by feeder between two dates.
@application.route('/getfeederamounts/<startdate>;<enddate>;<plcip>')
def getfeederamounts(startdate,enddate,plcip):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"

    #Open DB
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select output_number from pulses where plc_ip=\""+plcip+"\" group by OUTPUT_NUMBER"+";")
    outputs = cur.fetchall()
    rowarray_list=[]
    for output in outputs:
        cur.execute("select sum(pulseGiven) from pulses where plc_ip=\""+plcip+"\"and OUTPUT_NUMBER=\""+str(output[0])+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\""+";")
        rows = cur.fetchall()
        for row in rows:
            if row[0] is None:
                rowarray_list.append({'sum':'0','output_number':'0'})
            else:
                rowarray_list.append({'sum':row[0],'output_number':str(output[0])})
            if not rows:
                rowarray_list.append({'sum':'0','output_number':'0'})
    db.close()
    conn.close()
    cur.close() 
    return jsonify(rowarray_list)

@application.route('/getfoodsa/')
def getfoodsa():
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select * from foodsa;")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'food':'0','plcip':'0','timestamp':'0','user':'0','name':'0','company':'0','size':'0','medicated':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2].strftime("%Y-%m-%d %H:%M:%S"),'user':row[3],'name':row[4],'company':row[5],'size':row[6],'medicated':row[7]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'food':'0','plcip':'0','timestamp':'0','user':'0','name':'0','company':'0','size':'0','medicated':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getcompany/<actualfood>')
def getcompany(actualfood):
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select company,size,medicated from foodsa where name=\'"+actualfood+"\'"+";")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'company':'0','size':'0','medicated':'0'})
        else:
            rowarray_list.append({'company':row[0],'size':row[1],'medicated':row[2]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'company':'0','size':'0','medicated':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamount/<startdate>;<enddate>;<plcip>;<output_number>')
def getfeederamount(startdate,enddate,plcip,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    #Open DB
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"

    cur.execute("select sum(pulsegiven) from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0'})
        else:
            rowarray_list.append({'sum':row[0]})
    conn.close()
    cur.close()
    if not rows:
        rowarray_list.append({'sum':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamounts2', methods=["POST"])
def getfeederamounts2():
    plcip = request.json['plcip']
    startdate = request.json['startdate']
    enddate = request.json['enddate']
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select sum(pulseGiven),OUTPUT_NUMBER,strftime('%Y',timestamp),strftime('%m', timestamp),strftime('%d', timestamp) from pulses\
     where plc_ip=\""+plcip+"\" and timestamp between \""+startdate+"\" and \""+enddate+""+"\""+\
    " group by strftime('%Y', timestamp), strftime('%m', timestamp), strftime('%d', timestamp), OUTPUT_NUMBER;")
    rows = cur.fetchall()
    db.close() 
    return jsonify(rows)

@application.route('/getfeederamountsbyday/<startdate>;<enddate>;<plcip>')
def getfeederamountsbyday(startdate,enddate,plcip):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select sum(pulsegiven),output_number,to_char(timestamp,'%Y'),to_char(timestamp,'%m'),to_char(timestamp,'%d') from pulses\
     where plc_ip=\'"+plcip+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+""+"\'"+\
    " group by to_char(timestamp,'%Y'), to_char(timestamp, '%m'), to_char(timestamp, '%d'), output_number;")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0','output_number':'0','year':'0','month':'0','day':'0'})
        else:
            rowarray_list.append({'sum':row[0],'output_number':row[1],'year':row[2],'month':row[3],'day':row[4]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'sum':'0','output_number':'0','year':'0','month':'0','day':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamountsbydayandoutput/<startdate>;<enddate>;<plcip>;<output_number>')
def getfeederamountsbydayandoutput(startdate,enddate,plcip,output_number):
    result = []
    partialend = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    startdate_dt = datetime.strptime(startdate, '%Y-%m-%d %H:%M:%S')
    enddate_dt = datetime.strptime(enddate, '%Y-%m-%d %H:%M:%S')
    partialend_dt = datetime.strptime(partialend, '%Y-%m-%d %H:%M:%S')
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    while True:
        cur.execute("select sum(pulsegiven) from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between \'"+startdate_dt.strftime("%Y-%m-%d %H:%M:%S")+"\' and \'"+partialend_dt.strftime("%Y-%m-%d %H:%M:%S")+"\';")
        rows = cur.fetchone()
        if rows[0] is None:
            result.append({'sum':'0'})
        else:
            result.append({'sum':rows[0]})
        partialend_dt = partialend_dt + timedelta(days=1)
        startdate_dt = startdate_dt + timedelta(days=1)
        if not rows:
            result.append({'sum':'0'})
        if partialend_dt>enddate_dt:
            break
    cur.close()
    conn.close() 
    return jsonify(result)

@application.route('/getrationtimes/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationtimes(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select min(timestamp),max(timestamp) from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or row[1] is None:
            rowarray_list.append({'mindate':'0','maxdate':'0'})
        else:
            rowarray_list.append({'mindate':str(datetime.strptime(str(row[0]), '%Y-%m-%d %H:%M:%S')),'maxdate':str(datetime.strptime(str(row[1]), '%Y-%m-%d %H:%M:%S'))})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'mindate':'0','maxdate':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getresets/<startdate>;<plcip>;<output_number>')
def getresets(startdate,plcip,output_number):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select timestamp from resets where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\' order by timestamp asc;")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'timestamp':'0'})
        else:
            rowarray_list.append({'timestamp':row[0].strftime("%Y-%m-%d %H:%M:%S")})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'timestamp':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getrationamount/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationamount(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select sum(pulsegiven) from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0'})
        else:
            rowarray_list.append({'sum':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'sum':'0'})
    return jsonify(rowarray_list)

@application.route('/getrationok/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationok(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select rationok from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\' order by timestamp desc limit 1;")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'rationOk':'0'})
        else:
            rowarray_list.append({'rationOk':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'rationOk':'0'})
    return jsonify(rowarray_list)

@application.route('/getalarms/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getalarms(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select count(pondok) from states where pondok=\'0\' and plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'alarms':'0'})
        else:
            rowarray_list.append({'alarms':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'alarms':'0'})
    return jsonify(rowarray_list)

@application.route('/getonoff/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getonoff(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select count(onoff) from states where onOff=\'0\' and plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'onOff':'0'})
        else:
            rowarray_list.append({'onOff':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'onOff':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getstops/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getstops(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select count(rationstop) from states where rationStop=\'1\' and plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'rationStop':'0'})
        else:
            rowarray_list.append({'rationStop':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'rationStop':'0'})
    return jsonify(rowarray_list)

@application.route('/getfpalarms/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getfpalarms(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select count(fpok) from states where fpok=\'1\' and plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and ration=\'"+ration+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'fp_ok':'0'})
        else:
            rowarray_list.append({'fp_ok':row[0]})
    cur.close()
    conn.close()
    if not rows:
        rowarray_list.append({'fp_ok':'0'})
    return jsonify(rowarray_list)

@application.route('/getfoodlotlist/<startdate>')
def getfoodlotlist(startdate):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select distinct food from pulses where timestamp between \'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'food':'0'})
        else:
            rowarray_list.append({'food':row[0]})
    cur.close()
    conn.close() 
    if not rows:
        rowarray_list.append({'food':'0'})
    return jsonify(rowarray_list)

@application.route('/getfoodlotlastregisters/<plcip>;<output_number>')
def getfoodlotlastregisters(plcip,output_number):
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select timestamp,food,lot from foodslots where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' order by timestamp desc limit 1 ;")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'timestamp':'0','food':'0','lot':'0'})
        else:
            rowarray_list.append({'timestamp':row[0].strftime("%Y-%m-%d %H:%M:%S"),'food':row[1],'lot':row[2]})
    cur.close()
    conn.close() 
    if not rows:
        rowarray_list.append({'timestamp':'0','food':'0','lot':'0'})
    return jsonify(rowarray_list)

@application.route('/getamountbyfood/<startdate>;<plcip>;<output_number>')
def getamountbyfood(startdate,plcip,output_number):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select sum(pulsegiven),food from pulses where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between \'"+startdate+"\' and \'"+enddate+"\' group by food;")
    rows = cur.fetchall()
    print rows
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'sum':'0','food':'0'})
        else:
            rowarray_list.append({'sum':row[0],'food':row[1]})
    cur.close()
    conn.close()
    if not rows:
         rowarray_list.append({'sum':'0','food':'0'})
    return jsonify(rowarray_list)

@application.route('/getrates/<startdate>;<enddate>;<plcip>;<output_number>')
def getrates(startdate,enddate,plcip,output_number):
    enddate = enddate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select * from rates where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between\'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'id':'0','plcip':'0','timestamp':'0','user':'0','output_number':'0','rate':'0','rateBefore':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2].strftime("%Y-%m-%d %H:%M:%S"),'user':row[3],'output_number':row[4],'rate':row[5],'rateBefore':row[6]})
    cur.close()
    conn.close() 
    return jsonify(rowarray_list)

@application.route('/getdetailstates/<startdate>;<enddate>;<plcip>;<output_number>')
def getdetailstates(startdate,enddate,plcip,output_number):
    enddate = enddate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    cur.execute("select * from estadofp where plc_ip=\'"+plcip+"\' and output_number=\'"+output_number+"\' and timestamp between\'"+startdate+"\' and \'"+enddate+"\';")
    rows = cur.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'id':'0','plcip':'0','timestamp':'0','user':'0','output_number':'0','automatic':'0','on_program':'0','photoCell':'0','on_ok':'0','off_ok':'0','engaged_with':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2].strftime("%Y-%m-%d %H:%M:%S"),'user':row[3],'output_number':row[4],'automatic':row[5],'on_program':row[6],'photoCell':row[7],'on_ok':row[8],'off_ok':row[9],'engaged_with':row[10]})
    cur.close()
    conn.close() 
    return jsonify(rowarray_list)

@application.route('/setnewplc/<newplcip>')
def setnewplc(newplcip):
    result = []
    res = ""
    try:
        with open(FILEPATH+"plc_ips.txt", "a+") as f_plc:
            f_plc.write(newplcip+"\n")
            f_plc.close()
            result.append({'Result':'Ok'})
            res = "{'Result':'Ok'}"
    except:
        result.append({'Result':'Error'})
        res = "{'Result':'Error'}"
    print result
    return jsonify(result)

@application.route('/deleteplc/<plcip>')
def deleteplc(plcip):
    result = []
    try:
        f = open(FILEPATH+"plc_ips.txt","r")
        lines = f.readlines()
        f.close()
        f = open(FILEPATH+"plc_ips.txt","w")
        for line in lines:
            if line!=plcip+"\n":
                f.write(line)
        f.close()
        result.append({'Result':'Ok'})
    except IOError as e:
        result.append({'Result':'Error'})
    return jsonify(result)

#@application.route('/hola/<var>')
#def hola(var):
    #return "var"

@application.route('/hola', methods=['POST'])
def hola():
    #Open DB
    try:
        conn = psycopg2.connect("dbname='reports_database' user='postgres' host='localhost' password='postgres'")
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
    #print (request.is_json)
    #content = request.get_json()
    print request.get_data()
    print (len(request.get_data()))
    try:
        js = json.loads(request.get_data())
        print (js)
        print ""
        #print js['Config'][0]
        #data = js['Estanque']['DATA'].split(';rt')
        if js['POND']['Save_Data']=='pulses':
            print "Inserting Pulses"
            cur.execute('INSERT INTO pulses (plc_ip,timestamp,output_number,ration,pulsegiven,rationok,food) VALUES (%s, %s, %s, %s, %s, %s, %s)', (js['PLC'],js['POND']['Timestamp'],js['POND']['Id'],js['POND']['Ration'],js['POND']['PulseGiven'],js['POND']['RationOK'],""))

        elif js['POND']['Save_Data']=='rate':
            print "Inserting Rates"
            cur.execute('INSERT INTO rates(plc_ip,timestamp,user_,output_number,rate,ratebefore) VALUES (%s,%s,%s,%s,%s,%s)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Rate'],js['POND']['RateBeforeString']))
        elif js['POND']['Save_Data']=='rations':
            print "Inserting Rations"
            cur.execute('INSERT INTO rations(plc_ip,timestamp,user_,output_number,ration,starttime,endtime,rationamount,stoptime,cycleamount,cyclepulses,timebetweenpulses,percentage) \
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'\
                                ,(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Ration'],js['POND']['StartTime'],js['POND']['EndTime'],js['POND']['RationAmount'],js['POND']['StopTime'],js['POND']['CycleAmount'],js['POND']['CyclePulses'],js['POND']['TimeBetweenPulses'],js['POND']['Percentage']))
        elif js['POND']['Save_Data']=='states':
            print "Inserting States"
            cur.execute('INSERT INTO states(plc_ip,timestamp,user_,output_number,ration,onoff,rationstop,pondok,rationactive,fpok) \
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Ration'],js['POND']['onOff'],js['POND']['RationStop'],js['POND']['PondOkString'],js['POND']['RationActiveString'],js['POND']['FpOk']))
        elif js['POND']['Save_Data']=='estadofp':
            print "Inserting EstadoFp"
            cur.execute('INSERT INTO estadofp(plc_ip,timestamp,user_,output_number,automatic,onprogram,photocell,onok,offok,engagedwith) \
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Automatic'],js['POND']['On'],js['POND']['Photocell'],js['POND']['AlarmOn'],js['POND']['AlarmOff'],js['POND']['Engaged']))
        elif js['POND']['Save_Data']=='lightprog':
            print "Inserting LightProg"
            cur.execute('INSERT INTO lightprog(plc_ip,timestamp,user_,output_number,starttime,stoptime) \
                                VALUES (%s,%s,%s,%s,%s,%s)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['StartTime'],js['POND']['StopTime']))
        #Commit changes to DB.
        conn.commit()
    except Exception as e:
        print e
    cur.close()
    conn.close()
    return 'ok'


if __name__ == '__main__':
    application.run(host= '0.0.0.0',debug=True)
