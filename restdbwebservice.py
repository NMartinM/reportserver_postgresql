from flask import Flask, request, jsonify, json
from datetime import datetime
from datetime import timedelta
import os
import sqlite3

FILEPATH = ""

application = Flask(__name__)

@application.route('/listpulses/"<startdate>"-"<enddate>"')
def listpulses(startdate,enddate):
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select * from pulses where timestamp between \""+startdate+"\" and \""+enddate+"\"")
    rows = cursor.fetchall()
    db.close() 
    return jsonify(rows)

@application.route('/listfoodslots/"<startdate>"-"<enddate>"')
def listfoodslots(startdate,enddate):
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select * from foodslots where timestamp between \""+startdate+"\" and \""+enddate+"\"")
    rows = cursor.fetchall()
    db.close() 
    return jsonify(rows)

@application.route('/insertfoodslots/<plcip>;<timestamp>;<user>;<output_number>;<food>;<lot>')
def insertfoodslots(plcip,timestamp,user,output_number,food,lot):
    result=[]
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute('INSERT OR IGNORE INTO foodslots(plc_ip,timestamp,user,OUTPUT_NUMBER,food,lot) VALUES (?,?,?,?,?,?)',(plcip,timestamp,user,output_number,food,lot))
    #rows = cursor.fetchall()  
    print "foodslots"
    db.commit() #Commit changes to DB.
    db.close() #Close DB.

    result.append({'result':'ok'})
    return jsonify(result)

@application.route('/insertfoodsa/<plcip>;<timestamp>;<user>;<name>;<company>;<size>;<medicated>')
def insertfoodsa(plcip,timestamp,user,name,company,size,medicated):
    size = size.replace("_","/")
    result=[]
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute('INSERT OR IGNORE INTO foodsa(plc_ip,timestamp,user,name,company,size,medicated) VALUES (?,?,?,?,?,?,?)',(plcip,timestamp,user,name,company,size,medicated))
    #rows = cursor.fetchall()
    db.commit()
    db.close()
    print "foodsa"
    result.append({'result':'ok'})
    return jsonify(result)

#Returns the sum by feeder between two dates.
@application.route('/getfeederamounts/<startdate>;<enddate>;<plcip>')
def getfeederamounts(startdate,enddate,plcip):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select OUTPUT_NUMBER from pulses where plc_ip=\""+plcip+"\" group by OUTPUT_NUMBER"+";")
    outputs = cursor.fetchall()
    rowarray_list=[]
    for output in outputs:
        cursor.execute("select sum(pulseGiven) from pulses where plc_ip=\""+plcip+"\"and OUTPUT_NUMBER=\""+str(output[0])+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\""+";")
        rows = cursor.fetchall()
        for row in rows:
            if row[0] is None:
                rowarray_list.append({'sum':'0','output_number':'0'})
            else:
                rowarray_list.append({'sum':row[0],'output_number':str(output[0])})
            if not rows:
                rowarray_list.append({'sum':'0','output_number':'0'})
    db.close() 
    return jsonify(rowarray_list)

@application.route('/getfoodsa/')
def getfoodsa():
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select * from foodsa;")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'food':'0','plcip':'0','timestamp':'0','user':'0','name':'0','company':'0','size':'0','medicated':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2],'user':row[3],'name':row[4],'company':row[5],'size':row[6],'medicated':row[7]})
    db.close()
    if not rows:
        rowarray_list.append({'food':'0','plcip':'0','timestamp':'0','user':'0','name':'0','company':'0','size':'0','medicated':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getcompany/<actualfood>')
def getcompany(actualfood):
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select company,size,medicated from foodsa where name=\""+actualfood+"\""+";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'company':'0','size':'0','medicated':'0'})
        else:
            rowarray_list.append({'company':row[0],'size':row[1],'medicated':row[2]})
    db.close()
    if not rows:
        rowarray_list.append({'company':'0','size':'0','medicated':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamount/<startdate>;<enddate>;<plcip>;<output_number>')
def getfeederamount(startdate,enddate,plcip,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 3000")
    cursor.execute("select sum(pulseGiven) from pulses where plc_ip=\""+plcip+"\"and output_number=\""+output_number+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0'})
        else:
            rowarray_list.append({'sum':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'sum':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamounts2', methods=["POST"])
def getfeederamounts2():
    plcip = request.json['plcip']
    startdate = request.json['startdate']
    enddate = request.json['enddate']
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select sum(pulseGiven),OUTPUT_NUMBER,strftime('%Y',timestamp),strftime('%m', timestamp),strftime('%d', timestamp) from pulses\
     where plc_ip=\""+plcip+"\" and timestamp between \""+startdate+"\" and \""+enddate+""+"\""+\
    " group by strftime('%Y', timestamp), strftime('%m', timestamp), strftime('%d', timestamp), OUTPUT_NUMBER;")
    rows = cursor.fetchall()
    db.close() 
    return jsonify(rows)

@application.route('/getfeederamountsbyday/<startdate>;<enddate>;<plcip>')
def getfeederamountsbyday(startdate,enddate,plcip):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select sum(pulseGiven),OUTPUT_NUMBER,strftime('%Y',timestamp),strftime('%m', timestamp),strftime('%d', timestamp) from pulses\
     where plc_ip=\""+plcip+"\" and timestamp between \""+startdate+"\" and \""+enddate+""+"\""+\
    " group by strftime('%Y', timestamp), strftime('%m', timestamp), strftime('%d', timestamp), OUTPUT_NUMBER;")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0','output_number':'0','year':'0','month':'0','day':'0'})
        else:
            rowarray_list.append({'sum':row[0],'output_number':row[1],'year':row[2],'month':row[3],'day':row[4]})
    db.close()
    if not rows:
        rowarray_list.append({'sum':'0','output_number':'0','year':'0','month':'0','day':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getfeederamountsbydayandoutput/<startdate>;<enddate>;<plcip>;<output_number>')
def getfeederamountsbydayandoutput(startdate,enddate,plcip,output_number):
    result = []
    partialend = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    startdate_dt = datetime.strptime(startdate, '%Y-%m-%d %H:%M:%S')
    enddate_dt = datetime.strptime(enddate, '%Y-%m-%d %H:%M:%S')
    partialend_dt = datetime.strptime(partialend, '%Y-%m-%d %H:%M:%S')
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    while True:
        cursor.execute("select sum(pulseGiven) from pulses where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and timestamp between \""+startdate_dt.strftime("%Y-%m-%d %H:%M:%S")+"\" and \""+partialend_dt.strftime("%Y-%m-%d %H:%M:%S")+"\";")
        rows = cursor.fetchone()
        if rows[0] is None:
            result.append({'sum':'0'})
        else:
            result.append({'sum':rows[0]})
        partialend_dt = partialend_dt + timedelta(days=1)
        startdate_dt = startdate_dt + timedelta(days=1)
        if not rows:
            result.append({'sum':'0'})
        if partialend_dt>enddate_dt:
            break
    db.close() 
    return jsonify(result)

@application.route('/getrationtimes/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationtimes(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select min(timestamp),max(timestamp) from pulses where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or row[1] is None:
            rowarray_list.append({'mindate':'0','maxdate':'0'})
        else:
            rowarray_list.append({'mindate':row[0],'maxdate':row[1]})
    db.close()
    if not rows:
        rowarray_list.append({'mindate':'0','maxdate':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getresets/<startdate>;<plcip>;<output_number>')
def getresets(startdate,plcip,output_number):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select timestamp from resets where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\" order by timestamp asc;")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'timestamp':'0'})
        else:
            rowarray_list.append({'timestamp':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'timestamp':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getrationamount/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationamount(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select sum(pulseGiven) from pulses where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'sum':'0'})
        else:
            rowarray_list.append({'sum':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'sum':'0'})
    return jsonify(rowarray_list)

@application.route('/getrationok/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getrationok(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select rationok from pulses where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\" order by timestamp desc limit 1;")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'rationOk':'0'})
        else:
            rowarray_list.append({'rationOk':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'rationOk':'0'})
    return jsonify(rowarray_list)

@application.route('/getalarms/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getalarms(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select count(pondOk) from states where pondOk=\"0\" and plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'alarms':'0'})
        else:
            rowarray_list.append({'alarms':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'alarms':'0'})
    return jsonify(rowarray_list)

@application.route('/getonoff/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getonoff(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select count(onOff) from states where onOff=\"0\" and plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'onOff':'0'})
        else:
            rowarray_list.append({'onOff':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'onOff':'0'}) 
    return jsonify(rowarray_list)

@application.route('/getstops/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getstops(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select count(rationStop) from states where rationStop=\"1\" and plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'rationStop':'0'})
        else:
            rowarray_list.append({'rationStop':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'rationStop':'0'})
    return jsonify(rowarray_list)

@application.route('/getfpalarms/<startdate>;<enddate>;<plcip>;<ration>;<output_number>')
def getfpalarms(startdate,enddate,plcip,ration,output_number):
    startdate = startdate+" 00:00:00"
    enddate = enddate+" 23:59:59"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select count(fp_ok) from states where fp_ok=\"1\" and plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and ration=\""+ration+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'fp_ok':'0'})
        else:
            rowarray_list.append({'fp_ok':row[0]})
    db.close()
    if not rows:
        rowarray_list.append({'fp_ok':'0'})
    return jsonify(rowarray_list)

@application.route('/getfoodlotlist/<startdate>')
def getfoodlotlist(startdate):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select distinct food from pulses where timestamp between \""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'food':'0'})
        else:
            rowarray_list.append({'food':row[0]})
    db.close() 
    if not rows:
        rowarray_list.append({'food':'0'})
    return jsonify(rowarray_list)

@application.route('/getfoodlotlastregisters/<plcip>;<output_number>')
def getfoodlotlastregisters(plcip,output_number):
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select timestamp,food,lot from foodslots where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" order by timestamp desc limit 1 ;")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None:
            rowarray_list.append({'timestamp':'0','food':'0','lot':'0'})
        else:
            rowarray_list.append({'timestamp':row[0],'food':row[1],'lot':row[2]})
    db.close() 
    if not rows:
        rowarray_list.append({'timestamp':'0','food':'0','lot':'0'})
    return jsonify(rowarray_list)

@application.route('/getamountbyfood/<startdate>;<plcip>;<output_number>')
def getamountbyfood(startdate,plcip,output_number):
    enddate = startdate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select sum(pulseGiven),food from pulses where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and timestamp between \""+startdate+"\" and \""+enddate+"\" group by food;")
    rows = cursor.fetchall()
    print rows
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'sum':'0','food':'0'})
        else:
            rowarray_list.append({'sum':row[0],'food':row[1]})
    db.close()
    if not rows:
         rowarray_list.append({'sum':'0','food':'0'})
    return jsonify(rowarray_list)

@application.route('/getrates/<startdate>;<enddate>;<plcip>;<output_number>')
def getrates(startdate,enddate,plcip,output_number):
    enddate = enddate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select * from rates where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and timestamp between\""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'id':'0','plcip':'0','timestamp':'0','user':'0','output_number':'0','rate':'0','rateBefore':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2],'user':row[3],'output_number':row[4],'rate':row[5],'rateBefore':row[6]})
    db.close() 
    return jsonify(rowarray_list)

@application.route('/getdetailstates/<startdate>;<enddate>;<plcip>;<output_number>')
def getdetailstates(startdate,enddate,plcip,output_number):
    enddate = enddate+" 23:59:59"
    startdate = startdate+" 00:00:00"
    db = sqlite3.connect(FILEPATH+'reports_database')
    cursor = db.cursor()
    cursor.execute("PRAGMA busy_timeout = 1000")
    cursor.execute("select * from estadofp where plc_ip=\""+plcip+"\" and output_number=\""+output_number+"\" and timestamp between\""+startdate+"\" and \""+enddate+"\";")
    rows = cursor.fetchall()
    rowarray_list=[]
    for row in rows:
        if row[0] is None or not row[0]:
            rowarray_list.append({'id':'0','plcip':'0','timestamp':'0','user':'0','output_number':'0','automatic':'0','on_program':'0','photoCell':'0','on_ok':'0','off_ok':'0','engaged_with':'0'})
        else:
            rowarray_list.append({'id':row[0],'plcip':row[1],'timestamp':row[2],'user':row[3],'output_number':row[4],'automatic':row[5],'on_program':row[6],'photoCell':row[7],'on_ok':row[8],'off_ok':row[9],'engaged_with':row[10]})
    db.close() 
    return jsonify(rowarray_list)

@application.route('/setnewplc/<newplcip>')
def setnewplc(newplcip):
    result = []
    res = ""
    try:
        with open(FILEPATH+"plc_ips.txt", "a+") as f_plc:
            f_plc.write(newplcip+"\n")
            f_plc.close()
            result.append({'Result':'Ok'})
            res = "{'Result':'Ok'}"
    except:
        result.append({'Result':'Error'})
        res = "{'Result':'Error'}"
    print result
    return jsonify(result)

@application.route('/deleteplc/<plcip>')
def deleteplc(plcip):
    result = []
    try:
        f = open(FILEPATH+"plc_ips.txt","r")
        lines = f.readlines()
        f.close()
        f = open(FILEPATH+"plc_ips.txt","w")
        for line in lines:
            if line!=plcip+"\n":
                f.write(line)
        f.close()
        result.append({'Result':'Ok'})
    except IOError as e:
        result.append({'Result':'Error'})
    return jsonify(result)

#@application.route('/hola/<var>')
#def hola(var):
    #return "var"

@application.route('/hola', methods=['POST'])
def hola():
    #Open DB
    db = sqlite3.connect('reports_database')
    db.isolation_level = None
    db.execute('pragma journal_mode=wal')
    cursor = db.cursor()
    #print (request.is_json)
    #content = request.get_json()
    print request.get_data()
    print (len(request.get_data()))
    try:
        js = json.loads(request.get_data())
        print (js)
        print ""
        #print js['Config'][0]
        #data = js['Estanque']['DATA'].split(';')
        if js['POND']['Save_Data']=='pulses':
            print "Inserting Pulses"
            cursor.execute('INSERT OR IGNORE INTO pulses(plc_ip,timestamp,OUTPUT_NUMBER,ration,pulseGiven,rationOk,food) \
                                    VALUES (?,?,?,?,?,?,?)',(js['PLC'],js['POND']['Timestamp'],js['POND']['Id'],js['POND']['Ration'],js['POND']['PulseGiven'],js['POND']['RationOK'],""))
        elif js['POND']['Save_Data']=='rate':
            print "Inserting Rates"
            cursor.execute('INSERT OR IGNORE INTO rates(plc_ip,timestamp,user,OUTPUT_NUMBER,rate,rateBefore) \
                                VALUES (?,?,?,?,?,?)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Rate'],js['POND']['RateBeforeString']))
        elif js['POND']['Save_Data']=='rations':
            print "Inserting Rations"
            cursor.execute('INSERT OR IGNORE INTO \
                                rations(plc_ip,timestamp,user,OUTPUT_NUMBER,ration,startTime,endTime,rationAmount,stopTime,cycleAmount,cyclePulses,timeBetweenPulses,percentage) \
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)'\
                                ,(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Ration'],js['POND']['StartTime'],js['POND']['EndTime'],js['POND']['RationAmount'],js['POND']['StopTime'],js['POND']['CycleAmount'],js['POND']['CyclePulses'],js['POND']['TimeBetweenPulses'],js['POND']['Percentage']))
        elif js['POND']['Save_Data']=='states':
            print "Inserting States"
            cursor.execute('INSERT OR IGNORE INTO states(plc_ip,timestamp,user,OUTPUT_NUMBER,ration,onOff,rationStop,pondOk,rationActive,fp_ok) \
                                VALUES (?,?,?,?,?,?,?,?,?,?)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Ration'],js['POND']['onOff'],js['POND']['RationStop'],js['POND']['PondOkString'],js['POND']['RationActiveString'],js['POND']['FpOk']))
        elif js['POND']['Save_Data']=='estadofp':
            print "Inserting EstadoFp"
            cursor.execute('INSERT OR IGNORE INTO estadofp(plc_ip,timestamp,user,OUTPUT_NUMBER,automatic,on_program,photoCell,on_ok,off_ok,engaged_with) \
                                VALUES (?,?,?,?,?,?,?,?,?,?)',(js['PLC'],js['POND']['Timestamp'],js['POND']['User'],js['POND']['Id'],js['POND']['Automatic'],js['POND']['OnProgram'],js['POND']['PhotoCell'],js['POND']['OnOk'],js['POND']['OffOk'],js['POND']['EngagedWith']))
        db.commit() #Commit changes to DB.
    except Exception as e:
        print e
    db.close() #Close DB.
    return 'ok'


if __name__ == '__main__':
    application.run(host= '0.0.0.0',debug=True)
