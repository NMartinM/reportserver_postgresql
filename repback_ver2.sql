--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

-- Started on 2018-10-09 10:32:30 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12395)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 16780)
-- Name: estadofp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estadofp (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    automatic integer,
    onprogram integer,
    photocell integer,
    onok integer,
    offok integer,
    engagedwith integer
);


ALTER TABLE public.estadofp OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16783)
-- Name: estadofp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estadofp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estadofp_id_seq OWNER TO postgres;

--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 182
-- Name: estadofp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estadofp_id_seq OWNED BY public.estadofp.id;


--
-- TOC entry 183 (class 1259 OID 16785)
-- Name: foodsa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.foodsa (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    name character varying,
    company character varying,
    size character varying,
    medicated integer
);


ALTER TABLE public.foodsa OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16791)
-- Name: foodsa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.foodsa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.foodsa_id_seq OWNER TO postgres;

--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 184
-- Name: foodsa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.foodsa_id_seq OWNED BY public.foodsa.id;


--
-- TOC entry 185 (class 1259 OID 16793)
-- Name: foodslots; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.foodslots (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    food character varying,
    lot character varying
);


ALTER TABLE public.foodslots OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16799)
-- Name: foodslots_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.foodslots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.foodslots_id_seq OWNER TO postgres;

--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 186
-- Name: foodslots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.foodslots_id_seq OWNED BY public.foodslots.id;


--
-- TOC entry 198 (class 1259 OID 16895)
-- Name: lightprog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lightprog (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    starttime integer,
    stoptime integer
);


ALTER TABLE public.lightprog OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16893)
-- Name: lightprog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lightprog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lightprog_id_seq OWNER TO postgres;

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 197
-- Name: lightprog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lightprog_id_seq OWNED BY public.lightprog.id;


--
-- TOC entry 187 (class 1259 OID 16801)
-- Name: pulses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pulses (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    output_number integer,
    ration integer,
    pulsegiven real,
    rationok integer,
    food character varying
);


ALTER TABLE public.pulses OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16807)
-- Name: pulses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pulses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pulses_id_seq OWNER TO postgres;

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 188
-- Name: pulses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pulses_id_seq OWNED BY public.pulses.id;


--
-- TOC entry 189 (class 1259 OID 16809)
-- Name: rates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rates (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    rate integer,
    ratebefore integer
);


ALTER TABLE public.rates OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16812)
-- Name: rates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rates_id_seq OWNER TO postgres;

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 190
-- Name: rates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rates_id_seq OWNED BY public.rates.id;


--
-- TOC entry 191 (class 1259 OID 16814)
-- Name: rations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rations (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    ration integer,
    starttime integer,
    endtime integer,
    rationamount integer,
    stoptime integer,
    cycleamount integer,
    cyclepulses integer,
    timebetweenpulses integer,
    percentage integer
);


ALTER TABLE public.rations OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16817)
-- Name: rations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rations_id_seq OWNER TO postgres;

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 192
-- Name: rations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rations_id_seq OWNED BY public.rations.id;


--
-- TOC entry 193 (class 1259 OID 16819)
-- Name: resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resets (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer
);


ALTER TABLE public.resets OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16822)
-- Name: resets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.resets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resets_id_seq OWNER TO postgres;

--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 194
-- Name: resets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.resets_id_seq OWNED BY public.resets.id;


--
-- TOC entry 195 (class 1259 OID 16824)
-- Name: states; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.states (
    id integer NOT NULL,
    plc_ip character varying,
    "timestamp" timestamp without time zone,
    user_ integer,
    output_number integer,
    ration integer,
    onoff integer,
    rationstop integer,
    pondok integer,
    rationactive integer,
    fpok integer
);


ALTER TABLE public.states OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16827)
-- Name: states_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.states_id_seq OWNER TO postgres;

--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 196
-- Name: states_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.states_id_seq OWNED BY public.states.id;


--
-- TOC entry 2076 (class 2604 OID 16829)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estadofp ALTER COLUMN id SET DEFAULT nextval('public.estadofp_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 16830)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodsa ALTER COLUMN id SET DEFAULT nextval('public.foodsa_id_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 16831)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodslots ALTER COLUMN id SET DEFAULT nextval('public.foodslots_id_seq'::regclass);


--
-- TOC entry 2084 (class 2604 OID 16898)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lightprog ALTER COLUMN id SET DEFAULT nextval('public.lightprog_id_seq'::regclass);


--
-- TOC entry 2079 (class 2604 OID 16832)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pulses ALTER COLUMN id SET DEFAULT nextval('public.pulses_id_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 16833)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rates ALTER COLUMN id SET DEFAULT nextval('public.rates_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 16834)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rations ALTER COLUMN id SET DEFAULT nextval('public.rations_id_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 16835)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resets ALTER COLUMN id SET DEFAULT nextval('public.resets_id_seq'::regclass);


--
-- TOC entry 2083 (class 2604 OID 16836)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states ALTER COLUMN id SET DEFAULT nextval('public.states_id_seq'::regclass);


--
-- TOC entry 2086 (class 2606 OID 16838)
-- Name: estadofp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estadofp
    ADD CONSTRAINT estadofp_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 16840)
-- Name: foodsa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodsa
    ADD CONSTRAINT foodsa_pkey PRIMARY KEY (id);


--
-- TOC entry 2094 (class 2606 OID 16842)
-- Name: foodslots_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodslots
    ADD CONSTRAINT foodslots_pkey PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 16844)
-- Name: pulses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pulses
    ADD CONSTRAINT pulses_pkey PRIMARY KEY (id);


--
-- TOC entry 2102 (class 2606 OID 16846)
-- Name: rates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rates
    ADD CONSTRAINT rates_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 16848)
-- Name: rations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rations
    ADD CONSTRAINT rations_pkey PRIMARY KEY (id);


--
-- TOC entry 2110 (class 2606 OID 16850)
-- Name: resets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resets
    ADD CONSTRAINT resets_pkey PRIMARY KEY (id);


--
-- TOC entry 2114 (class 2606 OID 16852)
-- Name: states_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states
    ADD CONSTRAINT states_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 16942)
-- Name: unifp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estadofp
    ADD CONSTRAINT unifp UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2096 (class 2606 OID 16966)
-- Name: unifs; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodslots
    ADD CONSTRAINT unifs UNIQUE (plc_ip, output_number, food, lot);


--
-- TOC entry 2092 (class 2606 OID 16956)
-- Name: unifsa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.foodsa
    ADD CONSTRAINT unifsa UNIQUE (plc_ip, name);


--
-- TOC entry 2100 (class 2606 OID 16871)
-- Name: unipu; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pulses
    ADD CONSTRAINT unipu UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2104 (class 2606 OID 16903)
-- Name: unira; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rates
    ADD CONSTRAINT unira UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2108 (class 2606 OID 16881)
-- Name: unirat; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rations
    ADD CONSTRAINT unirat UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2112 (class 2606 OID 16916)
-- Name: unires; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resets
    ADD CONSTRAINT unires UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2116 (class 2606 OID 16929)
-- Name: unist; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states
    ADD CONSTRAINT unist UNIQUE (plc_ip, "timestamp", output_number);


--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-10-09 10:32:30 -03

--
-- PostgreSQL database dump complete
--

