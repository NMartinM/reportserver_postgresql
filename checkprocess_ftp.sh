#!/bin/bash

if pgrep -a python | grep ftpdbupdate > /dev/null
then
    echo "Running"
else
    echo "Stopped"
    /usr/bin/python /home/pi/reportserver/ftpdbupdate.py 
fi
